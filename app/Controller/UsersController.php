<?php

App::uses('AppController', 'Controller');

class UsersController extends AppController {

    public $components = array('Session', 'Auth');
    public $name = 'Users';
    public $uses = array();

    //ビューを使わない場合コメントを外す
    //public $autoRender = false;

    public function index() {
	$this->Auth->logout();
	$this->set('user', $this->Auth->user());
    }

    //どのアクションが呼ばれてもはじめに実行される関数
    public function beforeFilter() {
	parent::beforeFilter();

	

	//未ログインでアクセスできるアクションを指定
	//これ以外のアクションへのアクセスはloginにリダイレクトされる規約になっている
	$this->Auth->allow('register', 'login');
    }

    public function login() {
	$this->Auth->logout();
	if ($this->request->is('post')) {
	    if ($this->Auth->login()) {
		// ログイン成功
		return $this->redirect('/main/index');
	    } else {
		$this->Session->setFlash('ログイン失敗');
	    }
	}
    }

    public function logout() {
	$this->Auth->logout();
	$this->redirect('/users/login');
    }

    public function register() {
	$id = $this->User->find('first', array("fields" => "MAX(username) as max_number"))[0]['max_number'] + 1;
	$this->set('id', $id);
	
	//$this->requestにPOSTされたデータが入っている
	//POSTメソッドかつユーザ追加が成功したら

	if ($this->request->is('post') && $this->User->save($this->request->data)) {
	    //ログイン
	    //$this->request->dataの値を使用してログインする規約になっている
	    $this->Auth->login();
	    $this->Session->setFlash('登録しました');
	}
    }

}
