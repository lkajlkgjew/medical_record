<?php

App::uses('AppController', 'Controller');

class MainController extends AppController {

    public $components = array('Session', 'Auth');
    public $name = 'Main';
    public $uses = array('Record');

    //ビューを使わない場合コメントを外す
    //public $autoRender = false;

    function index() {
	$this->set('user', $this->Auth->user());

	/* $str = 'main page';
	  $this->set('str', $str); */
    }

    function add_record() {
	$this->set('user', $this->Auth->user());

	if ($this->request->is('post')) {
	    if ($this->Record->save($this->request->data)) {
		$this->Session->setFlash('登録しました。');
	    }
	}
    }

    function view_record() {
	$array = $this->Record->find('all', array("conditions" => array('Record.username =' => $this->Auth->user()['username'])));
	$this->set('array', $array);
    }

    function edit_record($id) {
	if ($this->request->is('post')) {
	    if ($this->Record->save($this->request->data)) {
		$this->redirect('/main/view_record');
	    }
	} else {
	    $data = $this->Record->find('first', array("conditions" => array('Record.id =' => $id)))['Record'];
	    $this->set('id', $id);
	    $this->set('data', $data);
	    $this->set('user', $this->Auth->user());
	}
    }

}
