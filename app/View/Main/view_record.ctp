<table>
    <th>病名</th>
    <th>処方</th>
    <th>処置</th>
    <th>作成日</th>
    <th>編集</th>
    <?php foreach ($array as $data): ?>
        <tr>
	    <?php
	    foreach ($data as $key => $val) {

		print (
			'<td>' . str_replace("\n", '<br>', h($val['disease'])) . '</td>' .
			'<td>' . str_replace("\n", '<br>', h($val['prescription'])) . '</td>' .
			'<td>' . str_replace("\n", '<br>', h($val['treatment'])) . '</td>' .
			'<td>' . h($val['created']) . '</td>'
		);

		print '<td>' . $this->Html->Link('編集', '/main/edit_record/' . $val['id']) . '</td>';
	    }
	    ?>
        </tr>
    <?php endforeach; ?>
</table>