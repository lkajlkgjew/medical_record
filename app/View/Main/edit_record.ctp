<?php

print(
	$this->Form->create('Record') .
	$this->Form->input('username', array('type' => 'hidden', 'value' => $user['username'])) .
	$this->Form->input('id', array('type' => 'hidden', 'value' => $id)) .
	$this->Form->input('disease', array('label' => '病名', 'value'=>$data['disease'])) .
	$this->Form->input('prescription', array('label' => '処方', 'value'=>$data['prescription'])) .
	$this->Form->input('treatment', array('label' => '処置', 'value'=>$data['treatment'])) .
	$this->Form->input('created', array('type' => 'datetime', 'label' => '作成日', 'dateFormat' => 'YMD', 'timeFormat' => '24', 'monthNames' => false, 'empty' => false, 'interval' => 15, 'minYear' => 2012)) .
	$this->Form->end('登録')
);
