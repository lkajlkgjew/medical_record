<h1>新規登録</h1>
<?php
print(

	$this->Form->create('User') .
	'患者ID:' . $id .
	$this->Form->input('username', array('type' => 'hidden', 'value' => $id)) .
	$this->Form->input('password', array('type' => 'hidden', 'value' => '1234')) .
	$this->Form->input('name_first', array('label' => '姓')) .
	$this->Form->input('name_last', array('label' => '名')) .
	$this->Form->input('birthday', array('type' => 'datetime', 'label' => '生年月日', 'dateFormat' => 'YMD', 'timeFormat' => '24', 'monthNames' => false, 'empty' => false, 'interval' => 15, 'minYear' => 2012)) .
	$this->Form->input('sex', array('label' => '性別', 'options' => array('男', '女'))) .
	$this->Form->end('登録')
);
